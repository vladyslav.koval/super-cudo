
let header_burger = document.querySelector('.header__burger');
let header_menu = document.querySelector('.header__menu');
let back = document.querySelector('body');
let header_list = document.querySelector('.header__list');

header_burger.addEventListener('click', burger)

function burger (){
    header_burger.classList.toggle('active');
    header_menu.classList.toggle('active');
    back.classList.toggle('lock');
}

header_list.addEventListener('click', headerList)

function headerList (){
    header_list.classList.remove('active');
    back.classList.toggle('lock');
}
